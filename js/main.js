window.addEventListener(
  'DOMContentLoaded',
  () => {
    // random div rotation
    const sign = () => (Math.random() >= 0.5 ? -1 : 1);
    const rows = document.getElementsByClassName('row');
    for (let i = 0; i < rows.length; i++) {
      const spans = rows[i].querySelectorAll('span');
      for (let j = 0; j < spans.length; j++) {
        const angle = Math.random() * Math.PI * sign();
        spans[j].style.webkitTransform = `rotate(${angle}deg)`;
        spans[j].style.mozTransform = `rotate(${angle}deg)`;
        spans[j].style.msTransform = `rotate(${angle}deg)`;
        spans[j].style.oTransform = `rotate(${angle}deg)`;
        spans[j].style.transform = `rotate(${angle}deg)`;
      }
    }

    // random background color
    const palette = ['#e1f7d5', '#ffbdbd', '#c9c9ff', '#ffffff', '#f1cbff'];
    const color = palette[Math.floor(Math.random() * palette.length)];
    document.body.style.backgroundColor = color;
  },
  false
);
